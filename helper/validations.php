<?php

function validateRegister() {

    $nombre = $_POST['nombre'];
    if ( !validateNombre($nombre)) { return false; }

    $apellidos = $_POST['apellidos'];
    if ( !validateApellidos($apellidos)) { return false; }

    $fecha = $_POST['nacimiento'];
    if ( !validateEdad($fecha)) { return false; }

    $dni = $_POST['dni'];
    if ( !validateDni($dni)) { return false; }

    $tlf = $_POST['telefono'];
    if ( !validateTelefono($tlf)) { return false; }

    $email = $_POST['email'];
    if ( !validateEmail($email)) { return false; }

    $pass = $_POST['pass'];
    if ( !validatePass($pass)) { return false; }

    return true;
}

function validateNombre($nombre) {
    $pattern = '/[a-z ]/i';

    if (strlen($nombre) != preg_match_all($pattern, $nombre)) {
        return false;
    }
    return true;
}
function validateApellidos($apellidos) {
    $pattern = '/[a-z ]/i';

    if (strlen($apellidos) != preg_match_all($pattern, $apellidos)) {
        return false;
    }
    return true;
}

function validateEdad($fecha) {
    $year = $fecha[0];

    for ($i=1; $i<4; $i++) {
        $fecha*= 10;
        $fecha+= $fecha[$i];
    }

    if (2021 - $year < 18) return false;

    return true;
}

function validateDni($dni) {
    $letra = substr("TRWAGMYFPDXBNJZSQVHLCKE", $dni%23, 1);
    if ($letra == $dni[8]) {
        return true;
    }
    else {
        return false;
    }
}

function validateTelefono($tlf) {
    $pattern = '/^[6-7]/i';

    if (strlen($tlf) == 9) {
        if (preg_match_all($pattern, $tlf)) {
            return true;
        }
    }
    return false;
}

function validateEmail($email) {
    $pattern = '/^[A-Z0-9]+@[A-Z0-9]+\.[A-Z]/i';

    if (preg_match_all($pattern, $email)) {
        return true;
    }
    return false;
}

function validatePass($pass) {


    $patternMayus = '*[A-Z]*';
    $patternMinus = '*[a-z]*';
    $patternNums = '*[0-9]*';
    $patternSpec = '*[@#$%&=.-/\*+_,;<>()¿?¡!ºª]*';

    if (strlen($pass) < 8) return false;
    if (!preg_match($patternMayus, $pass)) return false;
    if (!preg_match($patternMinus, $pass)) return false;
    if (!preg_match($patternNums, $pass)) return false;
    if (!preg_match($patternSpec, $pass)) return false;

    return true;
}
