
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo _("Registro");?></title>
        <form method='get' action='' id='form_lang' >
            Select Language : <select name='lang' onchange='changeLang();' >
                <option value='es' <?php if(isset($_GET['lang']) && $_GET['lang'] == 'es'){ echo "selected"; } ?> >Castellano</option>
                <option value='ca' <?php if(isset($_GET['lang']) && $_GET['lang'] == 'ca'){ echo "selected"; } ?> >Català</option>
            </select>
        </form>
    </head>
    <body>



        <a href="init.php">Init</a>
        <a href="login.php">Login</a>
        <br><br>

        <h2><?php echo _("Registro");?></h2>

        <form action="../controller/controller.php" method="post">
            <p><?php echo _("Nombre ");?><input name="nombre" type="text"></p>
            <p><?php echo _("Apellidos ");?><input name="apellidos" type="text"></p>
            <p><?php echo _("Fecha de nacimiento ");?><input name="nacimiento" type="date"></p>
            <p><?php echo _("Sexo ");?><select name="sexo">
                    <option value="1">H</option>
                    <option value="2">M</option>
                </select></p>
            <p><?php echo _("Telefono ");?><input name="telefono" type="number"></p>
            <p>DNI <input name="dni" type="text"></p>
            <p>EMAIL <input name="email" type="text"></p>
            <p><?php echo _("Contraseña ");?><input name="pass" type="password"></p>
            <input name="control" value="register" type="hidden"/>
            <input name="submit" value="submit" type="submit"/>
        </form>
    
    </body>
    <script>
        function changeLang(){
            document.getElementById('form_lang').submit();
        }
    </script>
</html>

