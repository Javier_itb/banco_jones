<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Transfer</title>
    </head>

    <body>
        <a href="profile.php">Profile</a>
        <a href="welcome.php">Cuentas</a>
        <a href="query.php">Query</a>
        <a href="logout.php">Logout</a>
        <br><br>

        <?php
        session_start();
        if ($_SESSION['user'] != "") {
            echo "<br><br>Hola, " . $_SESSION['user'];
        } else {
            header("Location: ./login.php");
        }
        ?>

        <h2> * TRANSFER *</h2>

        <form action="../controller/controller.php" method="post">
            <select name="cuentas">

                <?php
                require_once('../model/CuentaModel.php');
                $accounts=getAccounts($_POST['dni']);
                for ($i=0; $i<sizeof($accounts) ;$i++){?>
                    <option ><?php echo $accounts[$i]["cuenta"] ?></option>
                <?php }?>
            </select>
            Cuenta destino: <input name="cuenta_destino" type="text" /><br>
            Cantidad: <input name="cantidad" type="text" /><br>
            <input name="submit" type="submit" value="Seleccionar"/>
            <input name="control" type="hidden" value="transfer"/>
        </form>
    </body>
</html>


