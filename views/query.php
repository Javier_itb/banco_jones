<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title> * Query *</title>
    </head>

    <body>
        <a href="welcome.php">Cuentas</a>
        <a href="transfer.php">Transfer</a>
        <a href="profile.php">Profile</a>
        <a href="logout.php">Logout</a>
        <br><br>
        <?php
        session_start();
        if ($_SESSION['user'] != "") {
            echo "<br><br>Hola, " . $_SESSION['user'];
        } else {
            header("Location: ./login.php");
        }
        ?>

        <h2>QUERY</h2>

        <select name="Filtrar">
            <option value="1">Todas</option>
            <option value="2">Recibidas</option>
            <option value="3">Enviadas</option>
        </select>
    </body>
</html>


<?php
session_start();
if (isset($_SESSION['saldo'])) {
    echo "Saldo " . $_SESSION['saldo'] . '<br/>';
}
if (isset($_SESSION['lista'])) {
    $movimientos=$_SESSION['lista'];
    echo '<table class="default" rules="all" frame="border">';
    echo '<tr>';
    echo '<th>origen</th>';
    echo '<th>destino</th>';
    echo '<th>hora</th>';
    echo '<th>cantidad</th>';
    echo '</tr>';
    for ($i=0;$i<count($movimientos);$i++){
        echo '<tr>';
        echo '<td>'.$movimientos[$i]['origen'].'</td>';
        echo '<td>'.$movimientos[$i]['destino'].'</td>';
        echo '<td>'.$movimientos[$i]['hora'].'</td>';
        echo '<td>'.$movimientos[$i]['cantidad'].'</td>';
        echo '</tr>';
    }
    echo '</table>';

}

?>


<form action="../controller/controller.php" method="post">

    <select name="cuentas">


        <?php
        require_once('../model/CuentaModel.php');
        $accounts=getAccounts($_POST['dni']);
        for ($i=0; $i<sizeof($accounts) ;$i++){?>
            <option ><?php echo $accounts[$i]["cuenta"] ?></option>
        <?php }?>
    </select>
    <input name="submit" type="submit" value="Seleccionar"/>
    <input name="control" type="hidden" value="query"/>
</form>


