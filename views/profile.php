<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Profile</title>
    </head>
    <body>

        <a href="welcome.php">Cuentas</a>
        <a href="query.php">Query</a>
        <a href="transfer.php">Transfer</a>
        <a href="logout.php">Logout</a>
        <br><br>

        <?php
        session_start();
        if ($_SESSION['user'] != "") {
            echo "<br><br>Hola, " . $_SESSION['user'];
        } else {
            header("Location: ./login.php");
        }
        ?>

        <h2> * Tu perfil *</h2>

        <p>DNI: <?php echo $_SESSION['user'] ?><p style="font-style: italic">(no se puede modificar)</p></p>
        <form action="../controller/controller.php" method="post" enctype="multipart/form-data">
            <p>EMAIL <input type="text" name="email"></p>
            <p>TELÉFONO <input type="number" name="telefono"></p>

            <br><p> -- CAMBIAR DE CONTRASEÑA --</p>
            <p>CONTRASEÑA ACTUAL <input name="oldpass" type="password" /></p>
            <p>CONTRASEÑA NUEVA <input name="newpass" type="password" /></p>
            <p>CONFIRMA NUEVA CONTRASEÑA <input name="rnewpass" type="password" /></p>

            <br><p> -- IMAGEN --</p>
            <input type="file" name="upload">
            <input type="hidden" name="control" value="profile">
            <input type="submit" value="submit" name="submit">
        </form>
    </body>
</html>
