<?php

require_once('../db/DBManager.php');
use DBManager;

function insertCliente($cliente) {
    $manager = new DBManager();
    try {
        $sql = "INSERT INTO cliente (nombre, apellidos, fecha_nacimiento, sexo, telefono, dni, email, password) VALUES (:nombre, :apellidos, :fecha_nacimiento, :sexo, :telefono, :dni, :email, :password)";
        $nombre = $cliente->getNombre();
        $apellidos = $cliente->getApellidos();
        $fecha_nacimiento = $cliente->getFechaNacimiento();
        $sexo = $cliente->getSexo();
        $telefono = $cliente->getTelefono();
        $dni = $cliente->getDni();
        $email = $cliente->getEMail();
        $password = password_hash($cliente->getPassword(), PASSWORD_DEFAULT, ['cost'=>10]);

        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':nombre',$nombre);
        $stmt->bindParam(':apellidos',$apellidos);
        $stmt->bindParam(':fecha_nacimiento',$fecha_nacimiento);
        $stmt->bindParam(':sexo',$sexo);
        $stmt->bindParam(':telefono',$telefono);
        $stmt->bindParam(':dni',$dni);
        $stmt->bindParam(':email',$email);
        $stmt->bindParam(':password',$password);

        if ($stmt->execute()) {
            echo "OK";
        } else {
            echo "MAL";
        }

        /*
        if ( password_verify('123456', '123456')) {
            echo 'Iguales<br/>';
        } else {
            echo 'Diferentes<br/>';
        }
        */
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function getUserHash($dni) {
    $conexion = new DBManager();
    try {
        $sql = "SELECT * FROM cliente WHERE dni =: dni";
        $stmt = $conexion->getConexion()->prepare($sql);
        $stmt->bindParam(':dni', $dni);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result[0]['password'];
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}
