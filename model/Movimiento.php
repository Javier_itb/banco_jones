<?php

class Movimiento {
    private $origen;
    private $destino;
    private $hora;
    private $cantidad;

    /**
     * Movimiento constructor.
     * @param $origen
     * @param $destino
     * @param $hora
     * @param $cantidad
     */
    public function __construct($origen, $destino, $hora, $cantidad)
    {
        $this->origen = $origen;
        $this->destino = $destino;
        $this->hora = $hora;
        $this->cantidad = $cantidad;
    }

    /**
     * @return mixed
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * @param mixed $origen
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;
    }

    /**
     * @return mixed
     */
    public function getDestino()
    {
        return $this->destino;
    }

    /**
     * @param mixed $destino
     */
    public function setDestino($destino)
    {
        $this->destino = $destino;
    }

    /**
     * @return mixed
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * @param mixed $hora
     */
    public function setHora($hora)
    {
        $this->hora = $hora;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }


}