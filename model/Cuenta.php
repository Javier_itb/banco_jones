<?php

class Cuenta {
    private $id_cliente;
    private $cuenta;
    private $saldo;
    private $creacion;

    /**
     * Cuenta constructor.
     * @param $id_cliente
     * @param $cuenta
     * @param $saldo
     * @param $creacion
     */
    public function __construct($id_cliente, $cuenta, $saldo, $creacion)
    {
        $this->id_cliente = $id_cliente;
        $this->cuenta = $cuenta;
        $this->saldo = $saldo;
        $this->creacion = $creacion;
    }

    /**
     * @return mixed
     */
    public function getIdCliente()
    {
        return $this->id_cliente;
    }

    /**
     * @param mixed $id_cliente
     */
    public function setIdCliente($id_cliente)
    {
        $this->id_cliente = $id_cliente;
    }

    /**
     * @return mixed
     */
    public function getCuenta()
    {
        return $this->cuenta;
    }

    /**
     * @param mixed $cuenta
     */
    public function setCuenta($cuenta)
    {
        $this->cuenta = $cuenta;
    }

    /**
     * @return mixed
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @param mixed $saldo
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;
    }

    /**
     * @return mixed
     */
    public function getCreacion()
    {
        return $this->creacion;
    }

    /**
     * @param mixed $creacion
     */
    public function setCreacion($creacion)
    {
        $this->creacion = $creacion;
    }



}