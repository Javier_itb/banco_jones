<?php

use Cliente;
require('../model/Cliente.php');
require('../model/ClienteModel.php');
require('../helper/validations.php');


if ( isset($_POST['submit'])) {
    if ($_POST['control'] == 'register') {
        if (!validateRegister()) {
            header('Location: ../views/register.php');
        } else {
            $cliente = new Cliente($_POST['nombre'], $_POST['apellidos'], $_POST['nacimiento'], $_POST['sexo'], $_POST['telefono'], $_POST['dni'], $_POST['email'], $_POST['pass']);
            insertCliente($cliente);
            header('Location: ../views/profile.php');
        }
    }

    if ( $_POST['control'] == 'login' ) {
        $hash=getUserHash($_POST['dni']);
        if ( password_verify($_POST['password'],$hash)){
            session_start();
            $_SESSION['user']=$_POST['dni'];
//            $_SESSION['name']=$_POST['nombre'];
            header('Location: ../views/welcome.php');
        }else{
            require_once('../views/login.php');
        }
    }

    if ( $_POST['control']=='profile') {
        //if (validateProfile()){
        $check = getimagesize($_FILES['upload']['tmp_name']);
        $fileName = $_FILES['upload']['name'];
        echo $fileName . '<br/>';
        $fileSize = $_FILES['upload']['size'];
        $fileType = $_FILES['upload']['type'];
        $image = file_get_contents($_FILES['upload']['tmp_name']);
        error_log('init_profile');
        if ($check !== false) {
            updateCliente($image);
            $data = getImage();
            ob_start();
            fpassthru($data);
            $im = ob_get_contents();
            ob_end_clean();

            echo "<br/><img src='data:image/*;base64," . base64_encode($im) . "'/>";
        }
    }

    if($_POST['control']=='create') {
        session_start();
        createAccount($_SESSION['user']);
    }

    if($_POST['control']=='select_account') {
        $saldo = getSaldo($_POST['cuentas']);
        session_start();
        $_SESSION['saldo']=$saldo;
        $_SESSION['lista']=getMovimientos($_POST['cuentas']);
        header("Location: ../views/query.php");
    }

    if($_POST['control']=='query') {
        session_start();
        $_SESSION['saldo']=getSaldo($_POST['cuentas']);
        $_SESSION['lista']=getMovimientos($_POST['cuentas']);
        header("Location: ../views/query.php");
    }

    if($_POST['control']=='transfer') {
        if (existeCuenta($_POST['cuentas']) && existeCuenta($_POST['cuenta_destino'])) {
            transfer($_POST['cuentas'], $_POST['cuenta_destino'],$_POST['cantidad']);
        }
    }
}
